<?php

function createDeepArrayOfNumbers(int $deep): array {
    // Реализуйте функцию, которая случайным образом создает массив из X элементов,
    // состоящий из случайных целых чисел и из массивов целых чисел.
    // Глубина массива - $deep.
    // X должно быть больше 5 и меньше 10
    // Числа должны находиться в диапозоне от 10 до 10000

    $arr = [];
    for ($i = 0; $i < $deep; $i++) {
        for($i=0; $i<=rand(6, 8); $i++) {
            // Если мы не достигли глубины массива, то мы хотим создать новый массив,
            // иначе заполняем его числами
                if ($deep - 1 !== 0) {
                    $arr[] = createDeepArrayOfNumbers($deep - 1);
                }
                else{
                    $arr[] = rand(10, 10000);
                 }
        }
    }

    return $arr;
}

function calculateSum(array $deepArrayOfNumbers): int {
    // Напишите функцию которая вычисляет сумму чисел всех элементов и подэлементов структуры,
    // создаваемой функцией createDeepArrayOfNumbers.

    // если элемент число, складываем, массив - отправляем считаться заново
    $sum = 0;
    foreach ($deepArrayOfNumbers as $value) {
        if (is_int($value)){
            $sum += $value;
        }
        elseif(is_array($value)){
            $sum += calculateSum($value);
        }
    }

    return $sum;
}